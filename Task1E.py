# -*- coding: utf-8 -*-
"""
Created on Tue Jan 24 14:12:19 2017

@author: rache
"""

from floodsystem.geo import rivers_by_station_number
from floodsystem.stationdata import build_station_list

def run():
    stations = build_station_list()
    print(rivers_by_station_number(stations, 9))
    
if __name__ == "__main__":
    print("*** Task 1E: CUED Part IA Flood Warning System ***")
    run()