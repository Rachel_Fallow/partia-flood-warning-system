#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan 22 19:07:30 2017

@author: Baigent
"""

from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_by_distance

def run():
    #Get list of stations by distance from geo.py
    
    #List of all stations
    stations = build_station_list()
    #List of tuples of all stations and distance to Cambridge
    station_distance = stations_by_distance(stations, (52.2053, 0.1218))
    print(station_distance[:10])
    print(station_distance[-10:])
    
if __name__ == "__main__":
    print("*** Task 1B: CUED Part IA Flood Warning System ***")
    run()
    