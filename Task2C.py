# -*- coding: utf-8 -*-
"""
Created on Sun Jan 29 19:08:10 2017

@author: rache
"""

from floodsystem.flood import stations_highest_rel_level
from floodsystem.stationdata import build_station_list, update_water_levels

def run():
    stations = build_station_list()
    update_water_levels(stations)
    tuplist = stations_highest_rel_level(stations, 10)
    for x in tuplist:
        print(x[0], ":", x[1])
        
if __name__ == "__main__":
    print("*** Task 2C: CUED Part IA Flood Warning System ***")
    run()
    