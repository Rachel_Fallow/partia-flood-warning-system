# -*- coding: utf-8 -*-
"""
Created on Tue Jan 31 07:17:38 2017

@author: rache
"""
import datetime
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.analysis import polyfit
from floodsystem.datafetcher import fetch_measure_levels

def run():
    stations = build_station_list()
    update_water_levels(stations)
    info = fetch_measure_levels(stations[2].measure_id, dt=datetime.timedelta(days=1))
    return polyfit(info[0], info[1], 3)
    
print(run())
        