# -*- coding: utf-8 -*-
"""
Created on Sat Feb 25 11:41:17 2017

@author: rache
"""

"""This program returns two lists of the towns judged as being most at risk of
flooding. The first list is a list of towns with a station producing
inconsistent data. The second list is a list of towns which either have a 
relative level over a threshold of 1.5, or have a relative level over a
threshold of 1 and the water level is predicted to rise further"""


from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.station import MonitoringStation
from floodsystem.flood import station_level_over_threshold2
from floodsystem.analysis import polyfit
from floodsystem.datafetcher import fetch_measure_levels
import numpy as np
import matplotlib as mpl
import datetime
stations = build_station_list()
update_water_levels(stations)

def high_risk_towns(stations):
    
    #Create and print list of towns with malfunctioning stations
    list1 = []
    for station in stations:
        if MonitoringStation.typical_range_consistent(station) == False:
            if station.town in list1 == True:
                pass
            else:
                list1.append(station.town)
        
    print("The following towns are near a station which is not functioning: ", list1 )
    print("Please check stations and resolve issues")
    
    #Create and print list of towns with high risk relative level
    list2 = []
    thresh15 = station_level_over_threshold2(stations, 1.5)
    thresh1 = station_level_over_threshold2(stations, 1)
    for station in thresh1:
        if station in thresh15 == True:
            thresh1.pop(station)
        else:
            dt = 2
            dates, levels = fetch_measure_levels(station.measure_id,
                                         dt=datetime.timedelta(days=dt))
            datefloats = np.sort([mpl.dates.date2num(date) for date in dates])

            try:
                poly  = polyfit(datefloats, levels, 5)[0]
                dpoly = np.polyder(poly)
                if dpoly(datefloats[-1]) > 0:
                    thresh15.append(station)
            except:
                pass
                
    for station in thresh15:
        list2.append(station.town)
    print("The following towns have high flood risk: ", list2)
    
if __name__ == "__main__":
    print("*** Task 2G: CUED Part IA Flood Warning System ***")

    # Run Task 2G
    high_risk_towns(stations)
    
    