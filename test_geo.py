# -*- coding: utf-8 -*-
"""
Created on Tue Jan 24 08:13:31 2017

@author: rache
"""
from floodsystem.stationdata import build_station_list

def test_call():
    from floodsystem.geo import stations_within_radius
    #test that function is callable
    stations = build_station_list
    x = stations_within_radius(stations, (52.2053, 0.1218), 10)

def test_haversine():
    from floodsystem.geo import haversine
    hav = haversine((51.0782, 4.0583), (52.2053, 0.1218))
    #test that computed result matches result I have calculated to 1dp
    expect = 299.1
    assert(round(hav, 1) == expect)
    
def test_rivers_by_station_number():
    from floodsystem.geo import rivers_by_station_number
    stations = build_station_list
    #test that list produced has at least N entries
    testlist1 = rivers_by_station_number(stations, 15)
    testlist2 = rivers_by_station_number(stations, 3)
    testlist3 = rivers_by_station_number(stations, 40)
    assert len(testlist1) >= 15
    assert len(testlist2) >= 3
    assert len(testlist3) >= 40
    assert len(testlist1) != len(testlist2)
    assert len(testlist1) != len(testlist3)