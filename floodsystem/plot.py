'''Submodule for producing plot functions'''

import matplotlib as mpl
import matplotlib.pyplot as plt
from floodsystem.analysis import polyfit
import numpy as np
from floodsystem.station import MonitoringStation


def plot_water_levels(station, dates, levels):
    t = dates
    level = levels
    
    # Plot
    plt.plot(t, level)
    
    # Add axis labels, rotate date labels and add plot title
    plt.xlabel('Time')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45);
    plt.title(station)
    
    # Display plot
    plt.tight_layout()  # This makes sure plot does not cut off date labels
    plt.show()
    
    
    
def plot_water_level_with_fit(station, dates, levels, p):
    #if length of dates or levels == 0, do nothing
    if len(dates) == 0 or len(levels) == 0 or type(polyfit) == None:
        print('{} giving incorect data'.format(station.name))
        
    else:
        #plot data points for dates and levels
        x = np.sort([mpl.dates.date2num(date) for date in dates])
        y = levels
        shifted = [i - x[0] for i in x]
        plt.plot(shifted, y, '.')

    
        #plot best fit polynomial
        x1 = np.linspace(shifted[0], shifted[-1], 30)

        poly, d0 = polyfit(x, y, p)
        plt.plot(x1, poly(x1))
    
        #plot typical range
        if MonitoringStation.typical_range_consistent(station) == True:
            x2 = [station.typical_range[0]]*30
            x3 = [station.typical_range[1]]*30
            plt.plot(x1, x2)
            plt.plot(x1, x3)
        
        #Add Axis Lables
        plt.xlabel('Time from start (days)')
        plt.ylabel('water level (m)/ Best Fit Curve')
        plt.xticks(rotation=45);
        plt.title(station.name)
    
        plt.show()
        print("Shift of time axis = {} days since beginning of Gegorian Calendar".format(d0))