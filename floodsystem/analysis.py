# -*- coding: utf-8 -*-
"""
Created on Mon Jan 30 19:07:28 2017

@author: rache
"""

import numpy as np

def polyfit(dates, levels, p):
    """This function takes inputs of a list of dates, a list of levels and the
    desired order of the polynomial. It returns a tuple of (polynomial, date axis
    shift)"""
    #if length of dates or levels == 0, do nothing
    if len(dates) == 0 or len(levels) == 0:
            pass
    else:
        x = dates
        y = levels
        p_coeff = np.polyfit((x-x[0]), y, p)

    #to accommodate for poly not being referenced before attempted assignment
    #this would happen if the if statement above were fulfilled
    try:
        #convert into usable polynomial function
        poly = np.poly1d(p_coeff)
        return poly, x[0]
        
    except:
        pass