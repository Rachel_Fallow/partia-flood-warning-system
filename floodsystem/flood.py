# -*- coding: utf-8 -*-
"""
Created on Wed Jan 25 06:09:34 2017

@author: rache
"""
from floodsystem.station import MonitoringStation
from floodsystem.stationdata import update_water_levels, build_station_list
from floodsystem.utils import sorted_by_key


def station_level_over_threshold(stations, tol):
    """This function returns a list of stations with relative water level over a given
tolerance as (station name, relative level) tuples sorted into descending order
of water level."""
    #create empty list
    list1 = []
    stations = build_station_list()
    update_water_levels(stations)
    #test water level of each station and add to list if appropriate
    for station in stations:
        if MonitoringStation.relative_water_level(station) == None:
            pass
        elif MonitoringStation.relative_water_level(station) >= tol:
            a = (station.name, MonitoringStation.relative_water_level(station))
            list1.append(a)
        else:
            pass
    
    #sort in descending ourder of rwl
    list1.sort(key=lambda tup: tup[1], reverse=True)
    return list1
    
def station_level_over_threshold2(stations, tol):
    """This function returns a list of stations with relative water level over a given
tolerance as (station name, relative level) tuples sorted into descending order
of water level."""
    #create empty list
    list1 = []
    stations = build_station_list()
    update_water_levels(stations)
    #test water level of each station and add to list if appropriate
    for station in stations:
        if MonitoringStation.relative_water_level(station) == None:
            pass
        elif MonitoringStation.relative_water_level(station) >= tol:
            list1.append(station)
        else:
            pass
    return list1
    
def stations_highest_rel_level(stations, N):
    """This function returns a list of the N stations with the highest relative
    water level as (station name, relative level) tuples"""
    #create empty list
    list2 = []
    update_water_levels(stations)
    for station in stations:
        #ignore inconsistent stations
        if MonitoringStation.relative_water_level(station) == None:
            pass
        else:
            #add tuple to list
            a = (station.name, MonitoringStation.relative_water_level(station))
            list2.append(a)
            #sort list into descending order
            list2 = sorted_by_key(list2, 1, reverse = True)
    #return first N tuples in list
    return list2[:N]
    
