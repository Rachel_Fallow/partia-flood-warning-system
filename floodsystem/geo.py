"""This module contains a collection of functions related to
geographical data.

"""
from math import radians, cos, sin, asin, sqrt
from .utils import sorted_by_key
from .stationdata import build_station_list
AVG_EARTH_RADIUS = 6371  # in km


def haversine(point1, point2, miles = False):
    """ Calculate the great-circle distance between two points on the Earth surface.
    :input: two 2-tuples, containing the latitude and longitude of each point
    in decimal degrees.
    Example: haversine((45.7597, 4.8422), (48.8567, 2.3508))
    :output: Returns the distance bewteen the two points.
    The default unit is kilometers. Miles can be returned
    if the ``miles`` parameter is set to True.
    """
    # unpack latitude/longitude
    lat1, lng1 = point1
    lat2, lng2 = point2

    # convert all latitudes/longitudes from decimal degrees to radians
    lat1, lng1, lat2, lng2 = map(radians, (lat1, lng1, lat2, lng2))

    # calculate haversine
    lat = lat2 - lat1
    lng = lng2 - lng1
    d = sin(lat * 0.5) ** 2 + cos(lat1) * cos(lat2) * sin(lng * 0.5) ** 2
    h = 2 * AVG_EARTH_RADIUS * asin(sqrt(d))
    if miles == True:
        return h * 0.621371  # in miles
    else:
        return h  # in kilometers

        

def stations_by_distance(stations, p):
    #Function to sort stations by distance to location p
    #Empty list of tuples created
    stations_distance = []
    #Add statoin name and distance in miles to p for each station
    for station in stations:
        stations_distance.append((station.name, station.town, haversine(station.coord,p,True)))
    #Sort by distance (second entry in tuple)
    stations_distance = sorted_by_key(stations_distance,2)
    #Return list of all stations and distance
    return stations_distance
    
def rivers_with_stations(stations):
    #Function to return rivers associated with given stations
    #Create empty set for results
    rivers = set()
    #Add each river name to set (duplicates not allowed in a set)
    for station in stations:
        rivers.add(station.river)
    #Sort river names alphabetically
    rivers = list(rivers)
    rivers.sort()
    return rivers
    
def stations_by_river(stations):
    #Function to return dicionary of rivers to stations
    #Create empty dictionary 'dict'
    dicto = {}
    #Create list of rivers using function rivers_with_stations
    rivers = rivers_with_stations(stations)
    #For each river in rivers, extract stations and add them to dict
    for river in rivers:
        dicto[river] = []
        #Iterate through each station in stations and add name if it is on the river
        for station in stations:
            if station.river == river:
                dicto[river].append(station.name)
    return dicto
    
def stations_within_radius(stations, centre, r):
    stations = build_station_list()
    
    #create list of stations within radius of centre
    stations_within_r = [station.name for station in stations if haversine(station.coord, centre) <= r]

    #sort list alphabetically
    stations_within_r.sort()
    return stations_within_r


"""Function that determines the N rivers with the greatest number of monitoring
 stations. It returns a list of (river name, number of stations) tuples, 
 sorted by the number of stations. In the case that there are more rivers with 
 the same number of stations as the N th entry, these rivers are included in the 
 list."""
 
 
def rivers_by_station_number(stations, N):
    stations = build_station_list()
    number_of_stations = {} #create empty dictionary to store no of stations for each river 
    for station in stations: #Fill Dictionary with rivers + no of stations
        riv = station.river
        if riv in number_of_stations:
            number_of_stations[riv] += 1
        else:
            number_of_stations[riv] = 1
    nos_tuples = list(number_of_stations.items()) #convert to list of tuples
    nos_tuples.sort(key=lambda tup: tup[1], reverse=True)#put in descending order
    while nos_tuples[N-1][1] == nos_tuples[N][1]:#include rivers with same number
        N += 1
    return nos_tuples[:N] #return top N rivers