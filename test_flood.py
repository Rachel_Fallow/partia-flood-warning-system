# -*- coding: utf-8 -*-
"""
Created on Sun Jan 29 18:11:23 2017

@author: rache
"""

from floodsystem.stationdata import build_station_list
from floodsystem.stationdata import update_water_levels
from floodsystem.station import MonitoringStation
from floodsystem.flood import stations_highest_rel_level

def test_types():
    #check that variables being used in 2B are of the correct type
    stations = build_station_list()
    assert type(stations) == list
    update_water_levels(stations)
    for station in stations:
        if MonitoringStation.relative_water_level(station) == None:
            pass
        else:
            assert type(MonitoringStation.relative_water_level(station)) == float

def test_2C_length():
    #Test that list produces by stations_highest_rel_level is correct length
    stations = build_station_list()
    update_water_levels(stations)
    Nlist = [0, 1, 5, 27, 43]
    for N in Nlist:
        a = stations_highest_rel_level(stations, N)
        assert len(a) == N
