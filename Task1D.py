from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_with_stations
from floodsystem.geo import stations_by_river


def run1():
    #Function to run fist part of 1D
    stations = build_station_list()
    #Run rivers function on all stations
    rivers = rivers_with_stations(stations)
    #Print length of set and first 10 results
    print('Number of rivers:',len(rivers))
    print(rivers[:10])
    
def run2():
    #Function to run second part of 1D
    stations = build_station_list()
    #Run stations by river function on all stations
    all_rivers = stations_by_river(stations)
    aire = all_rivers['River Aire']
    aire.sort()
    cam = all_rivers['River Cam']
    cam.sort()
    thames = all_rivers['Thames']
    thames.sort()
    print(aire)
    print(cam)
    print(thames)

    
if __name__ == "__main__":
    print("*** Task 1D: CUED Part IA Flood Warning System ***")    
    run1()
    run2()
