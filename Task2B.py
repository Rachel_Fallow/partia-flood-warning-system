# -*- coding: utf-8 -*-
"""
Created on Wed Jan 25 06:32:49 2017

@author: rache
"""

from floodsystem.flood import station_level_over_threshold
from floodsystem.stationdata import build_station_list
from floodsystem.stationdata import update_water_levels

def run():
    stations = build_station_list()
    update_water_levels(stations)
    tuplist = station_level_over_threshold(stations, 0.8)
    for x in tuplist:
        print(x[0], ":", x[1])
    
if __name__ == "__main__":
    print("*** Task 2B: CUED Part IA Flood Warning System ***")
    run()

