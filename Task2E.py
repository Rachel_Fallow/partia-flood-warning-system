'''File to run Task2E'''
import datetime
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.utils import sorted_by_key
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.plot import plot_water_levels


def run():
    # Build list of stations
    stations = build_station_list()
    # Update latest level data for all stations
    update_water_levels(stations)
    #Empty list of tuples for names and current levels
    name_level = []
    #Add tuple of name and current level
    for station in stations:
        if station.latest_level == None:
            pass
        else:
            name_level.append((station.name, station.latest_level))
    #Sort by level, highest to lowest
    sorted_by_key(name_level, 1, True)
    #List of names of 5 highest levels
    highest = []    
    for station in name_level[:5]:
        highest.append(station[0])
    #list 'highest' now contains only names of 5 stations with highest current levels
    print('Highest current levels at:', highest)
    
    #Build new list of desired details for these 5 stations
    required = []
    for station in stations:
        if station.name in highest:
            dt = 10 
            dates, levels = fetch_measure_levels(station.measure_id,
                                         dt=datetime.timedelta(days=dt))
            #Create empty tuple for string of date
            dates_str = []
            
            for date in dates:
                dates_str.append(str(date))
                
            #Build list of tuples containing name, dates (as string), levels for past 10 days
            required.append((station.name, dates, levels))  
    
    #Plot required stations
    for station in required:
       plot_water_levels(station[0],station[1],station[2])

run()

