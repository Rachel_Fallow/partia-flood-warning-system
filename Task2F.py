# -*- coding: utf-8 -*-
"""
Created on Mon Feb 13 07:04:22 2017

@author: rache
"""
import datetime
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.station import MonitoringStation
from floodsystem.plot import plot_water_level_with_fit
from floodsystem.utils import sorted_by_key


def run():
    stations = build_station_list()
    update_water_levels(stations)
    dt = 2
 
    list2 = []
    for station in stations:
        #ignore inconsistent stations
        if MonitoringStation.relative_water_level(station) == None:
            pass
        else:
            #add tuple to list
            a = (station, MonitoringStation.relative_water_level(station))
            list2.append(a)
            #sort list into descending order
            list2 = sorted_by_key(list2, 1, reverse = True)
            highest5 = list2[:5]
 
    for station in highest5:
        station = station[0]
        if station.name == 'Bampton Grange':
            pass
        else:
            dates, levels = fetch_measure_levels(station.measure_id,
                                         dt=datetime.timedelta(days=dt))
            plot_water_level_with_fit(station, dates, levels, 4)

    
if __name__ == "__main__":
    print("*** Task 2F: CUED Part IA Flood Warning System ***")

    run()
