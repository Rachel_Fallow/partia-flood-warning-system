# -*- coding: utf-8 -*-

from floodsystem.stationdata import build_station_list
from floodsystem.station import inconsistent_typical_range_stations
#Demonstrate 1F which returns all stations with inconsistent range data

def run():
    #List of all stations
    stations = build_station_list()
    incon = inconsistent_typical_range_stations(stations)
    incon.sort()
    print(incon)
    
if __name__ == "__main__":
    print("*** Task 1F: CUED Part IA Flood Warning System ***")
    run()